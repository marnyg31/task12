﻿using System;
using System.Collections.Generic;
using Task12_EntityFramwork.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Newtonsoft.Json;
using System.IO;

namespace Task12_EntityFramwork
{
    class Program
    {
        static void Main(string[] args)
        {
            //main loop to manage school business 
            bool done = false;
            using (var db = new SchoolDbContext())
            {
                WriteDbToJson(db);
                while (!done)
                {
                    Console.Clear();
                    Console.WriteLine("do you want to manage?\n" +
                                      "(s) student\n(p) professor\n" +
                                      "(c) certificates\n\n(q) quit");
                    var key = Console.ReadKey();
                    if (key.KeyChar == 'q') done = true;
                    else if (key.KeyChar == 's') Student.Menu(db);
                    else if (key.KeyChar == 'p') Professor.Menu(db);
                    // else if (key.KeyChar == 'd') PaymentDetails.Menu(db);
                    else if (key.KeyChar == 'c') Certification.Menu(db);
                }
            }
        }

        private static void WriteDbToJson(SchoolDbContext db)
        {
            List<Professor> wholeDb = db.Professors
            .Include(p => p.Payment)
            .Include(p => p.Students)
            .Include(p => p.ProfessorCertifications)
            .ThenInclude(pc => pc.Certification)
            .ToList();
            string jsonString= JsonConvert.SerializeObject(
                wholeDb,
                Formatting.Indented,
                new JsonSerializerSettings(){ReferenceLoopHandling=ReferenceLoopHandling.Ignore}
                );
            File.WriteAllText("dbJSON.json",jsonString);
        }
    }
}
