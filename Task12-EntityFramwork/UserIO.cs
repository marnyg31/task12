﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12_EntityFramwork
{
    class UserIO
    {
        public static double ReadDoubleFromUser(string prompt)
        {
            Console.WriteLine(prompt);
            double GPA;
            while (!double.TryParse(Console.ReadLine(), out GPA))
                Console.WriteLine("please enter an number");
            return GPA;
        }

        public static DateTime ReadDateTimeFromUser(string prompt)
        {
            Console.WriteLine(prompt);
            DateTime dt;
            while (!DateTime.TryParseExact(Console.ReadLine(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dt))
                Console.WriteLine("Invalid date, please retry");
            return dt;
        }

        public static int ReadIntFromUser( string prompt)
        {
            Console.WriteLine(prompt);
            int studentId;
            while (!int.TryParse(Console.ReadLine(), out studentId))
                Console.WriteLine("please enter an int");
            return studentId;
        }

        public static string ReadStringFromUser(string prompt)
        {
            string str="";
            while (string.IsNullOrEmpty(str))
            {
                Console.WriteLine(prompt);
                str = Console.ReadLine();
            }
            return str;
        }
    }
}
