﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Task12_EntityFramwork.Models
{
    class PaymentDetails
    {

        //Id is PK for PaymentDetails table
        //Professor is the referens to the professor table(one to one) 
        public int Id { get; set; }
        public double Payment { get; set; }
        public Professor Professor{get;set;}


        public static void Menu(SchoolDbContext db)
        {
            //loop for managing PaymentDetails
            bool done = false;
            while (!done)
            {
                Console.Clear();
                PrintAllPaymentsDetails(db);
                Console.WriteLine("do you want to do?\n" +
                                  "(a) add, (m) modify,(d) delete empty, (q) quit");
                var key = Console.ReadKey();
                if (key.KeyChar == 'q') done = true;
                else if (key.KeyChar == 'a') AddNewPaymentDetail(db);
                else if (key.KeyChar == 'd') DeleteEmptyPaymentDetail(db);
                else if (key.KeyChar == 'm') ModifyPaymentDetail(db);
                db.SaveChanges();
            }
        }

        private static void ModifyPaymentDetail(SchoolDbContext db)
        {
            //function to modify PaymentDetails data
            PaymentDetails paymentDetails= GetPaymentDetailSelectionFromUser(db);
            paymentDetails.Payment=UserIO.ReadDoubleFromUser("Please enter the size of the payment");
        }

        private static void DeleteEmptyPaymentDetail(SchoolDbContext db)
        {
            var paymentDetailsToDelete= db.PaymentDetailses.Where(pd=>pd.Professor==null);
            db.PaymentDetailses.RemoveRange(paymentDetailsToDelete);
        }

        private static PaymentDetails GetPaymentDetailSelectionFromUser(SchoolDbContext db)
        {
            //function to get a PaymentDetails object from user input
            PrintAllPaymentsDetails(db);
            PaymentDetails pd = null;
            while (pd == null)
            {
                int paymentDetailID = UserIO.ReadIntFromUser("please enter the PaymentDetail ID you want to modify:");
                pd = db.PaymentDetailses.Find(paymentDetailID);
                Console.WriteLine("that object does not exist");
            }
            Console.Clear();
            return pd;
        }

        // internal static PaymentDetails GetPaymentDetailsFromUser(SchoolDbContext db)
        // {
        //     PrintAllPaymentsDetails(db);
        //     System.Console.WriteLine("do you want to use an existing payment or type (a) to add new payment");
        //     //Console.ReadKey();
        //     //ask user to 
        //     throw new NotImplementedException();
        // }

        private static void AddNewPaymentDetail(SchoolDbContext db)
        {
            //function to add a new PaymentDetails to database
            double payment=UserIO.ReadDoubleFromUser("Pleas enter the size of the payment");
            db.PaymentDetailses.Add(new PaymentDetails(){Payment=payment});
        }
        private static void PrintAllPaymentsDetails(SchoolDbContext db)
        {
            //function to list all PaymentDetails in terminal
            Console.Clear();
            Console.WriteLine($"{"ID",-4}  {"Payment",-14}  {"For Professor",-20}");
            foreach (var paymentDets in db.PaymentDetailses.Include(p=>p.Professor))
                Console.WriteLine($"{paymentDets.Id,-4}  {paymentDets.Payment,-14}  {(paymentDets.Professor==null ? "" : paymentDets.Professor.Name), -20}");
        }
    }
}
