﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Task12_EntityFramwork.Models
{
    class Certification
    {
        //Id is pk of the certification table
        //ProfessorCertifications is a joining class for professor and certification 
        //This is necessary to achieve a many to many connection
        //IE, one professor can have many certificates, and one certificate can have many professors 

        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ProfessorCertification> ProfessorsCertifications { get; set; }

        public static void Menu(SchoolDbContext db)
        {
            //main menu for managing certifications
            bool done = false;
            while (!done)
            {
                Console.Clear();
                PrintAllCerts(db);
                Console.WriteLine("do you want to do?\n" +
                                  "(a) add, (d) delete, (m) modify, (q) quit");
                var key = Console.ReadKey();
                if (key.KeyChar == 'q') done = true;
                else if (key.KeyChar == 'a') AddNewCert(db);
                else if (key.KeyChar == 'd') DeleteCert(db);
                else if (key.KeyChar == 'm') ModifyCert(db);
                db.SaveChanges();
            }
        }

        private static void ModifyCert(SchoolDbContext db)
        {
            //function to modify certification
            Certification certification = GetCertificationSelectionFromUser(db);
            if(certification==null){
                System.Console.WriteLine("There are no certifications\npress any key to return");
                Console.ReadKey();
                return;
            }
            certification.Name = UserIO.ReadStringFromUser("Please enter the a new name for the Certification");
        }

        private static void DeleteCert(SchoolDbContext db)
        {
            //function to delete certification
            Certification certification = GetCertificationSelectionFromUser(db);
            if(certification==null){
                System.Console.WriteLine("There are no certifications\npress any key to return");
                Console.ReadKey();
                return;
            }
            db.Certifications.Remove(certification);
        }

        private static void AddNewCert(SchoolDbContext db)
        {
            //function to add certification
            string name = UserIO.ReadStringFromUser("Please enter the a new name for the Certification");
            db.Certifications.Add(new Certification() { Name = name });
        }

        private static Certification GetCertificationSelectionFromUser(SchoolDbContext db)
        {
            //function to get a certification a object form user input
            if(db.Certifications.Count()<=0) return null;
            PrintAllCerts(db);
            Certification c = null;
            while (c == null)
            {
                int studentID = UserIO.ReadIntFromUser("please enter the Certification ID you want to modify:");
                c = db.Certifications.Find(studentID);
                Console.WriteLine("that certificate does not exist");
            }
            Console.Clear();
            return c;
        }

        private static void PrintAllCerts(SchoolDbContext db)
        {
            //function to list all certifications in terminal
            Console.Clear();
            Console.WriteLine($"{"ID",-4}  {"name",-10}");
            foreach (var cert in db.Certifications)
                Console.WriteLine($"{cert.Id,-4}  {cert.Name,-10}");
        }
    }
}
