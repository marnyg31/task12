﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Microsoft.EntityFrameworkCore;

namespace Task12_EntityFramwork.Models
{
    class Student
    {
        //Id is the PK for student table 
        //ProfessorId is the nullable FK to connect any given student to one or zero professor
        public int Id { get; set; }
        public DateTime DOB { get; set; }
        public string Name { get; set; }
        public double GradeAverage { get; set; }
        public int? ProfessorId { get; set; }
        public Professor Professor { get; set; }

        public static void Menu(SchoolDbContext db)
        {
            //main menu for managing students
            bool done = false;
            while (!done)
            {
                Console.Clear();
                PrintAllStudents(db);
                Console.WriteLine();
                Console.WriteLine("do you want to (a) add student, (m) modify students, (d) delete student, (q) return");
                var p = Console.ReadKey();
                if (p.KeyChar == 'a') AddStudent(db);
                if (p.KeyChar == 'm') ModifyStudent(db);
                if (p.KeyChar == 'd') DeleteStudent(db);
                if (p.KeyChar == 'q') done = true;
                db.SaveChanges();
            }
        }

        public static void ModifyStudent(SchoolDbContext db)
        {
            //menu for modifying student data
            var s = GetStudentSelectionFromUser(db);
            if (s==null){
                System.Console.WriteLine("there are no students\npress any key to return");
                Console.ReadKey();
                return;
            }
            bool done = false;
            while (!done)
            {
                Console.WriteLine($"{"ID",-4}  {"Name",-10}  {"GPA",-4}  {"DOB",-10}  {"Professor"}");
                Console.WriteLine($"{s.Id,-4}  {s.Name,-10}  {s.GradeAverage,-4}  {s.DOB.ToShortDateString(),-10}  {(s.Professor != null ? s.Professor.Name : "nan")}\n");
                Console.WriteLine("do you want to modify (n) Name, (a) DOB, (p) professor, (g) GPA, (q) return");
                var p = Console.ReadKey();
                Console.Clear();
                if (p.KeyChar == 'n') ModifyStudentName(db, s);
                if (p.KeyChar == 'a') ModifyStudentAge(db, s);
                if (p.KeyChar == 'p') ModifyProfessorOfStudent(db, s);
                if (p.KeyChar == 'g') ModifyGpaOfStudent(db, s);
                if (p.KeyChar == 'q') done = true;
                db.SaveChanges();
            }
        }

        private static void ModifyGpaOfStudent(SchoolDbContext db, Student s)
        {
            //function to modify gpa of student
            s.GradeAverage = UserIO.ReadDoubleFromUser("Enter new GPA of student");
        }

        private static void ModifyStudentAge(SchoolDbContext db, Student s)
        {
            //function to modify age of student
            s.DOB = UserIO.ReadDateTimeFromUser("Enter date of birth (dd/MM/yyyy)");
        }

        private static void ModifyStudentName(SchoolDbContext db, Student s)
        {
            //function to modify name of student
            s.Name = UserIO.ReadStringFromUser("Please enter the new name");
        }

        private static void ModifyProfessorOfStudent(SchoolDbContext db, Student s)
        {
            //function to modify professor of student
            if(db.Professors.Count()<=0){
                System.Console.WriteLine("there are no professors\npress any key to return");
                Console.ReadKey();
                return;
            }
            Models.Professor.PrintAllProfessors(db);
            s.ProfessorId = UserIO.ReadIntFromUser("please enter the number of the professor in charge of student:");
        }

        private static Student GetStudentSelectionFromUser(SchoolDbContext db)
        {
            //function to get a student object from user input
            if (db.Students.Count() <= 0) return null;
            PrintAllStudents(db);
            Student s = null;
            while (s == null)
            {
                int studentID = UserIO.ReadIntFromUser("please enter the student ID you want to modify:");
                s = db.Students.Find(studentID);
                Console.WriteLine("that student does not exist");
            }
            Console.Clear();
            return s;
        }

        public static void AddStudent(SchoolDbContext db)
        {
            //function to add a student to database
            string name = UserIO.ReadStringFromUser("Pleas enter a name");
            DateTime dt = UserIO.ReadDateTimeFromUser("Pleas enter a DOB (dd/MM/yyyy)");
            double GPA = UserIO.ReadDoubleFromUser("Pleas enter the students GPA");
            Models.Professor professor = Models.Professor.GetProfessorFromUser(db);
            db.Students.Add(new Student() { Name = name, DOB = dt, GradeAverage = GPA, Professor = professor });
        }

        internal static void PrintAllStudentsOfProfessor(SchoolDbContext db, Professor p)
        {
            //function to list all students of a professor in terminal
            var students = db.Students.Where(s => s.ProfessorId == p.Id).ToList();
            Console.Clear();
            Console.WriteLine($"{"ID",-4}  {"Name",-10}  {"GPA",-4}  {"DOB",-10}");
            foreach (var student in students)
                Console.WriteLine($"{student.Id,-4}  {student.Name,-10}  {student.GradeAverage,-4}  {student.DOB.ToShortDateString(),-10}");
        }

        internal static void PrintAllStudentsNotOfProfessor(SchoolDbContext db, Professor p)
        {
            //function to list all students of a professor in terminal
            var students = db.Students.Where(s => s.ProfessorId != p.Id).ToList();
            Console.Clear();
            Console.WriteLine($"{"ID",-4}  {"Name",-10}  {"GPA",-4}  {"DOB",-10}");
            foreach (var student in students)
                Console.WriteLine($"{student.Id,-4}  {student.Name,-10}  {student.GradeAverage,-4}  {student.DOB.ToShortDateString(),-10}");
        }
        private static void PrintAllStudents(SchoolDbContext db)
        {
            //function to list all students in terminal
            var students = db.Students.Include(s => s.Professor).ToList();
            Console.Clear();
            Console.WriteLine($"{"ID",-4}  {"Name",-10}  {"GPA",-4}  {"DOB",-10}  {"Professor"}");
            foreach (var student in students)
                Console.WriteLine($"{student.Id,-4}  {student.Name,-10}  {student.GradeAverage,-4}  {student.DOB.ToShortDateString(),-10}  {(student.Professor != null ? student.Professor.Name : "nan")}");
        }

        public static void DeleteStudent(SchoolDbContext db)
        {
            //function to delete a student from database
            var s = GetStudentSelectionFromUser(db);
            if (s==null){
                System.Console.WriteLine("there are no students to delete\npress any key to return");
                Console.ReadKey();
                return;
            }
            db.Students.Remove(s);
        }
    }
}
