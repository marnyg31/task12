﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Task12_EntityFramwork.Models
{
    class SchoolDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Certification> Certifications { get; set; }
        public DbSet<PaymentDetails> PaymentDetailses { get; set; }
       public DbSet<ProfessorCertification> ProfessorCertifications { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            SqlConnectionStringBuilder builder = GetStringBuilder();
            SqlConnectionStringBuilder GetStringBuilder()
            {
                SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
                sqlConnectionStringBuilder.DataSource = @"PC7581\SQLEXPRESS";
                sqlConnectionStringBuilder.InitialCatalog = "School";
                sqlConnectionStringBuilder.IntegratedSecurity = true;
                return sqlConnectionStringBuilder;
            }

            optionsBuilder.UseSqlServer(builder.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProfessorCertification>().HasKey(pc => new {pc.ProfessorId, pc.CertificationId});
        }
    }
}
