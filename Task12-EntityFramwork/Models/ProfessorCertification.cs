﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Task12_EntityFramwork.Models
{
    class ProfessorCertification
    {
        //binding class between professor table and certification table.
        //This is necessary to achieve a many to many connection.
        //this table can have many combinations of zero to many professors,
        //connected to zero to many certifications

        public int ProfessorId { get; set; }
        public Professor Professor { get; set; }
        public int CertificationId { get; set; }
        public Certification Certification { get; set; }

        internal static ProfessorCertification AddExistingCertificationToProfessor(SchoolDbContext db, Professor p)
        {
            //TODO doc/check/fix
            //if professor has all certificates do nothing
            //else list those and have user select one
            if (ProfessorHasAllCertificates(db, p)) return null;
            PrintAllCertsNotInProfessor(db, p);

            Certification certification = null;
            int id = -1;
            IQueryable<Certification> certsProfDoseNotHave = CetCertificatesProfessorDoesNotHave(db, p);
            while (certification == null)
            {
                id = UserIO.ReadIntFromUser("enter ID of cert to add to professor");
                certification = certsProfDoseNotHave.FirstOrDefault(c => c.Id == id);
                System.Console.WriteLine("try again");
            }
            Console.Clear();
            var l = new ProfessorCertification() { ProfessorId = p.Id, CertificationId = id };
            return l;
        }

        private static IQueryable<Certification> CetCertificatesProfessorDoesNotHave(SchoolDbContext db, Professor p)
        {
            var certsProfessorHas = db.ProfessorCertifications.Where(pc => pc.Professor == p).Select(pc => pc.Certification);
            var certsProfDoseNotHave = db.Certifications.Where(c => !certsProfessorHas.Contains(c));
            return certsProfDoseNotHave;
        }

        private static bool ProfessorHasAllCertificates(SchoolDbContext db, Professor p)
        {
            IQueryable<Certification> certsProfDoseNotHave = CetCertificatesProfessorDoesNotHave(db, p);
            return certsProfDoseNotHave.Count() <= 0;
        }

        public List<ProfessorCertification> GetCertificationsForProfessor(SchoolDbContext db, Professor prof)
        {
            //TODO doc/check/fix
            return db.ProfessorCertifications.Where(pc => pc.Professor == prof).Include(pc => pc.Certification).ToList();
        }

        public static void PrintAllCertsNotInProfessor(SchoolDbContext db, Professor p)
        {
            //function to list all certifications not already added to a professor
            Console.Clear();
            Console.WriteLine($"{"ID",-5}  {"name",-10}");
            IQueryable<Certification> certsProfDoseNotHave = CetCertificatesProfessorDoesNotHave(db, p);
            foreach (var cert in certsProfDoseNotHave)
                Console.WriteLine($"{cert.Id,-5}  {cert.Name,-10}");
        }
        public static void PrintAllCertsInProfessor(SchoolDbContext db, Professor p)
        {
            //function to list all certifications not already added to a professor
            Console.Clear();
            System.Console.WriteLine("Current certifications:");
            Console.WriteLine($"{"ID",-5}  {"name",-10}");
            var certsProfessorHas = db.ProfessorCertifications.Where(pc => pc.Professor == p).Select(pc => pc.Certification);
            foreach (var cert in certsProfessorHas)
                Console.WriteLine($"{cert.Id,-5}  {cert.Name,-10}");
        }
    }
}
