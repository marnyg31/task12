﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Task12_EntityFramwork.Models
{
    class Professor
    {
        //Id is PK for the professor table
        //
        //there is no refferens to students from professor table, since it is
        //a one to many relationship(IE, many students have a FK to one professor PK)
        //
        //ProfessorCertifications is a joining class for professor and certification 
        //This is necessary to achieve a many to many connection
        //IE, one professor can have many certificates, and one certificate can have many professors 
        //
        //PaymentID is the FK to the payment table, one professor has one payment
        public int Id { get; set; }
        public DateTime DOB { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public ICollection<ProfessorCertification> ProfessorCertifications { get; set; }
        public PaymentDetails Payment { get; set; }
        public int PaymentID { get; set; }

        public static void Menu(SchoolDbContext db)
        {
            //main menu for managing professors
            bool done = false;
            while (!done)
            {
                Console.Clear();
                PrintAllProfessors(db);
                Console.WriteLine();
                Console.WriteLine("do you want to (a) add professor, (m) modify professor,(l) list certificates of professor, (d) delete professor, (q) return");
                var p = Console.ReadKey();
                if (p.KeyChar == 'a') AddProfessor(db);
                if (p.KeyChar == 'm') ModifyProfessor(db);
                if (p.KeyChar == 'd') DeleteProfessor(db);
                if (p.KeyChar == 'l') ListCertificatesOfProfessor(db);
                if (p.KeyChar == 'q') done = true;
                db.SaveChanges();
            }
        }

        private static void ListCertificatesOfProfessor(SchoolDbContext db)
        {
            var p = GetProfessorFromUser(db);
            if (p == null)
            {
                System.Console.WriteLine("there are no professors\npress any key to return");
                Console.ReadKey();
                return;
            }
            Console.WriteLine($"{"ID",-4}  {"name",-10}");
            foreach (var cert in db.ProfessorCertifications.Include(cp => cp.Certification).Where(cp => cp.Professor == p).Select(cp => cp.Certification))
                Console.WriteLine($"{cert.Id,-4}  {cert.Name,-10}");
            System.Console.WriteLine("press any key to return");
            Console.ReadKey();
        }

        private static void DeleteProfessor(SchoolDbContext db)
        {
            //function to delete professor
            var p = GetProfessorFromUser(db);
            if (p == null)
            {
                System.Console.WriteLine("there are no professors\npress any key to return");
                Console.ReadKey();
                return;
            }
            db.PaymentDetailses.Remove(db.PaymentDetailses.Find(p.PaymentID));
            db.Professors.Remove(p);
        }

        public static void ModifyProfessor(SchoolDbContext db)
        {
            //menu to modify professor data
            var p = GetProfessorFromUser(db);
            if (p == null)
            {
                System.Console.WriteLine("there are no professors\npress any key to return");
                Console.ReadKey();
                return;
            }

            bool done = false;
            Student.PrintAllStudentsOfProfessor(db, p);
            while (!done)
            {
                Console.Clear();
                PrintAllProfessors(db);
                Console.WriteLine("What do you want to change (n) name, (d) DOB, (p) payment,(c) certifications,(s) students, (q) return");
                var key = Console.ReadKey();
                if (key.KeyChar == 'n') p.Name = UserIO.ReadStringFromUser("Enter new name");
                if (key.KeyChar == 'd') p.DOB = UserIO.ReadDateTimeFromUser("Enter new DOB");
                if (key.KeyChar == 'p') p.Payment = new PaymentDetails() { Payment = UserIO.ReadDoubleFromUser("Enter new payment") };
                if (key.KeyChar == 'c') ModifyCertifications(db, p);
                if (key.KeyChar == 's') ModifyStudents(db, p);
                if (key.KeyChar == 'q') done = true;
                db.SaveChanges();
            }
        }

        private static void ModifyStudents(SchoolDbContext db, Professor p)
        {
            //function to modify students of professor
            bool done = false;
            while (!done)
            {
                Console.Clear();
                Student.PrintAllStudentsOfProfessor(db, p);
                Console.WriteLine("do you want to (a) add students, (r) remove students, (q) return");
                var key = Console.ReadKey();
                if (key.KeyChar == 'a') AddExistingStudents(db, p);
                if (key.KeyChar == 'r') RemoveStudents(db, p);
                if (key.KeyChar == 'q') done = true;
                db.SaveChanges();
            }
        }
        private static void AddExistingStudents(SchoolDbContext db, Professor p)
        {
            Student.PrintAllStudentsNotOfProfessor(db, p);
            var studentsNotOfProfessor = db.Students.Where(s => s.ProfessorId != p.Id).ToList();
            if (studentsNotOfProfessor.Count <= 0)
            {
                System.Console.WriteLine("There are no students to add\npress any key to return");
                Console.ReadKey();
                return;
            }
            Student student = null;
            while (student == null)
            {
                int id = UserIO.ReadIntFromUser("enter ID of student to add to professor");
                student = studentsNotOfProfessor.FirstOrDefault(c => c.Id == id);
                System.Console.WriteLine("try again");
            }
            student.Professor = p;
            Console.Clear();
        }
        private static void RemoveStudents(SchoolDbContext db, Professor p)
        {
            Student.PrintAllStudentsOfProfessor(db, p);
            var studentsOfProfessor = db.Students.Where(s => s.ProfessorId == p.Id).ToList();
            if (studentsOfProfessor.Count <= 0)
            {
                System.Console.WriteLine("There are no students to remove\npress any key to return");
                Console.ReadKey();
                return;
            }
            Student student = null;
            while (student == null)
            {
                int id = UserIO.ReadIntFromUser("enter ID of student to add to professor");
                student = studentsOfProfessor.FirstOrDefault(c => c.Id == id);
                System.Console.WriteLine("try again");
            }
            student.Professor = null;
            Console.Clear();
        }
        private static void ModifyCertifications(SchoolDbContext db, Professor p)
        {
            //function to modify a professors certification
            bool done = false;
            while (!done)
            {
                Console.Clear();
                ProfessorCertification.PrintAllCertsInProfessor(db, p);
                Console.WriteLine("do you want to (a) add certification, (r) remove certification, (q) return");
                var key = Console.ReadKey();
                if (key.KeyChar == 'a') AddExistingCertification(db, p);
                if (key.KeyChar == 'r') RemoveCertificateFromProfessor(db, p);
                if (key.KeyChar == 'q') done = true;
                db.SaveChanges();
            }
        }

        private static void RemoveCertificateFromProfessor(SchoolDbContext db, Professor p)
        {
            //function to remove a certification from professor
            ProfessorCertification.PrintAllCertsInProfessor(db, p);
            if (p.ProfessorCertifications.Count() <= 0)
            {
                System.Console.WriteLine("there are no certificates to remove\npress any key to return");
                Console.ReadKey();
                return;
            }
            Certification certification = null;
            var certsProfessorHas = db.ProfessorCertifications.Where(pc => pc.Professor == p).Select(pc => pc.Certification);
            while (certification == null)
            {
                int id = UserIO.ReadIntFromUser("enter ID of cert to add to professor");
                certification = certsProfessorHas.FirstOrDefault(c => c.Id == id);
                System.Console.WriteLine("try again");
            }
            var toBeRemoved = db.ProfessorCertifications.Find(new object[] { p.Id, certification.Id });
            db.ProfessorCertifications.Remove(toBeRemoved);
            Console.Clear();
        }

        private static void AddExistingCertification(SchoolDbContext db, Professor p)
        {
            //function to add a certification to professor
            var certification = ProfessorCertification.AddExistingCertificationToProfessor(db, p);
            if (certification == null)
            {
                System.Console.WriteLine("professor already has all possible certifications");
                System.Console.WriteLine("press any key to return");
                Console.ReadKey();
            }
            else
            {
                p.ProfessorCertifications.Add(certification);
            }
        }

        private static void AddProfessor(SchoolDbContext db)
        {
            //function to add a new professor
            string name = UserIO.ReadStringFromUser("Pleas enter a name");
            DateTime dt = UserIO.ReadDateTimeFromUser("Pleas enter a DOB (dd/MM/yyyy)");
            PaymentDetails payment = new PaymentDetails() { Payment = UserIO.ReadDoubleFromUser("Enter payment for professor") };
            Professor p = new Professor() { DOB = dt, Name = name, Payment = payment };

            db.Professors.Add(p);
        }

        public static void PrintAllProfessors(SchoolDbContext db)
        {
            //function to list all professors in terminal
            Console.WriteLine($"{"ID",-4}  {"Name",-10}  {"DOB",-10}  {"payment",-10}");
            foreach (var professor in db.Professors.Include(p => p.Payment)) Console.WriteLine($"{professor.Id,-4}  {professor.Name,-10}  {professor.DOB.ToShortDateString(),-10}  {professor.Payment.Payment,-10}");
        }

        public static Professor GetProfessorFromUser(SchoolDbContext db)
        {
            //function to get a professor object from user input
            if (db.Professors.Count() <= 0) return null;
            Console.Clear();
            PrintAllProfessors(db);

            Professor p = null;
            while (p == null)
            {
                int profID = UserIO.ReadIntFromUser("please enter the ID of a professor:");
                p = db.Professors.Include(p => p.ProfessorCertifications).Include(p => p.Students).Where(p => p.Id == profID).FirstOrDefault();
                Console.WriteLine("that professor does not exist");
            }
            Console.Clear();
            return p;
        }
    }
}
