﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Task12_EntityFramwork.Models;

namespace Task12_EntityFramwork.Migrations
{
    public partial class InsertingTestData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "PaymentDetailses",
                columns: new[] {"Id", "Payment"},
                values: new object[,]
                {
                    {1, 10000.3},
                    {2, 1337.58008},
                    {3, 123321.123}
                });
            migrationBuilder.InsertData(
                table: "Certifications",
                columns: new[] {"Id", "Name"},
                values: new object[,]
                {
                    {1, "badass"},
                    {2, "dumbass"},
                    {3, "hardass"}
                });
            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] {"id", "Name", "DOB", "PaymentId"},
                values: new object[,]
                {
                    {1, "tim", new DateTime(2000, 10, 10), 1},
                    {2, "tom", new DateTime(2000, 10, 11), 2},
                    {3, "bil", new DateTime(1100, 12, 11), 3}
                });
            migrationBuilder.InsertData(
                table: "ProfessorCertification",
                columns: new[] {"ProfessorId", "CertificationId"},
                values: new object[,]
                {
                    {1, 1},
                    {1, 2},
                    {2, 2},
                    {2, 3},
                    {3, 1},
                    {3, 2},
                    {3, 3}
                });
            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] {"id", "Name", "DOB","GradeAverage", "ProfessorId"},
                values: new object[,]
                {
                    {1, "jack", new DateTime(2002, 10, 10),1.1, 1},
                    {2, "timmy", new DateTime(2019, 10, 11),6.0, 2},
                    {3, "timmy2", new DateTime(2019, 10, 11),6.0, 2},
                    {4, "timmy3", new DateTime(2019, 10, 11),6.0, 2},
                    {5, "billy", new DateTime(3000, 12, 11),3.0, 3}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "Id",
                keyValues: new object[] {1,2,3,4,5});
            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "Id",
                keyValues: new object[] {1,2,3});
            migrationBuilder.DeleteData(
                table: "Certifications",
                keyColumn: "Id",
                keyValues: new object[] {1,2,3});
            migrationBuilder.DeleteData(
                table: "PaymentDetailses",
                keyColumn: "Id",
                keyValues: new object[] {1,2,3});
        }
    }
}
