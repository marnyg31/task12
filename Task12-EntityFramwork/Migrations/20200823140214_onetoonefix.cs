﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12_EntityFramwork.Migrations
{
    public partial class onetoonefix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertification_Certifications_CertificationId",
                table: "ProfessorCertification");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertification_Professors_ProfessorId",
                table: "ProfessorCertification");

            migrationBuilder.DropForeignKey(
                name: "FK_Professors_PaymentDetailses_PaymentId",
                table: "Professors");

            migrationBuilder.DropIndex(
                name: "IX_Professors_PaymentId",
                table: "Professors");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorCertification",
                table: "ProfessorCertification");

            migrationBuilder.RenameTable(
                name: "ProfessorCertification",
                newName: "ProfessorCertifications");

            migrationBuilder.RenameColumn(
                name: "PaymentId",
                table: "Professors",
                newName: "PaymentID");

            migrationBuilder.RenameIndex(
                name: "IX_ProfessorCertification_CertificationId",
                table: "ProfessorCertifications",
                newName: "IX_ProfessorCertifications_CertificationId");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentID",
                table: "Professors",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorCertifications",
                table: "ProfessorCertifications",
                columns: new[] { "ProfessorId", "CertificationId" });

            migrationBuilder.CreateIndex(
                name: "IX_Professors_PaymentID",
                table: "Professors",
                column: "PaymentID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertifications_Certifications_CertificationId",
                table: "ProfessorCertifications",
                column: "CertificationId",
                principalTable: "Certifications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertifications_Professors_ProfessorId",
                table: "ProfessorCertifications",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_PaymentDetailses_PaymentID",
                table: "Professors",
                column: "PaymentID",
                principalTable: "PaymentDetailses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertifications_Certifications_CertificationId",
                table: "ProfessorCertifications");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertifications_Professors_ProfessorId",
                table: "ProfessorCertifications");

            migrationBuilder.DropForeignKey(
                name: "FK_Professors_PaymentDetailses_PaymentID",
                table: "Professors");

            migrationBuilder.DropIndex(
                name: "IX_Professors_PaymentID",
                table: "Professors");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorCertifications",
                table: "ProfessorCertifications");

            migrationBuilder.RenameTable(
                name: "ProfessorCertifications",
                newName: "ProfessorCertification");

            migrationBuilder.RenameColumn(
                name: "PaymentID",
                table: "Professors",
                newName: "PaymentId");

            migrationBuilder.RenameIndex(
                name: "IX_ProfessorCertifications_CertificationId",
                table: "ProfessorCertification",
                newName: "IX_ProfessorCertification_CertificationId");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentId",
                table: "Professors",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "PaymentDetailses",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorCertification",
                table: "ProfessorCertification",
                columns: new[] { "ProfessorId", "CertificationId" });

            migrationBuilder.CreateIndex(
                name: "IX_Professors_PaymentId",
                table: "Professors",
                column: "PaymentId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertification_Certifications_CertificationId",
                table: "ProfessorCertification",
                column: "CertificationId",
                principalTable: "Certifications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertification_Professors_ProfessorId",
                table: "ProfessorCertification",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_PaymentDetailses_PaymentId",
                table: "Professors",
                column: "PaymentId",
                principalTable: "PaymentDetailses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
