﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12_EntityFramwork.Migrations
{
    public partial class MoreModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PaymentId",
                table: "Professors",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Certifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Certifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentDetailses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Payment = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentDetailses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProfessorCertification",
                columns: table => new
                {
                    ProfessorId = table.Column<int>(nullable: false),
                    CertificationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfessorCertification", x => new { x.ProfessorId, x.CertificationId });
                    table.ForeignKey(
                        name: "FK_ProfessorCertification_Certifications_CertificationId",
                        column: x => x.CertificationId,
                        principalTable: "Certifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProfessorCertification_Professors_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Professors_PaymentId",
                table: "Professors",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfessorCertification_CertificationId",
                table: "ProfessorCertification",
                column: "CertificationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_PaymentDetailses_PaymentId",
                table: "Professors",
                column: "PaymentId",
                principalTable: "PaymentDetailses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Professors_PaymentDetailses_PaymentId",
                table: "Professors");

            migrationBuilder.DropTable(
                name: "PaymentDetailses");

            migrationBuilder.DropTable(
                name: "ProfessorCertification");

            migrationBuilder.DropTable(
                name: "Certifications");

            migrationBuilder.DropIndex(
                name: "IX_Professors_PaymentId",
                table: "Professors");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "Professors");
        }
    }
}
